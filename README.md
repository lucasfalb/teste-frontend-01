# Teste Frontend 01

Olá, muito obrigado por participar do processo de contratação da Seox. Este repositório contém as instruções de entrega do seu teste. Siga elas da forma mais precisa o possível para conseguirmos avaliar você de forma adequada.

## Por onde começar

1. Faça um fork deste repositório e utilize-o para realizar a implementação da solução.
2. Utilize o README para adicionar detalhamentos de como executar o seu projeto, além de comentários livres que você queira adicionar.

## Requisitos

Você deverá construir uma página responsiva seguindo as orientações visuais dos arquivos em Figma listados abaixo. Nele, você encontrará o layout da versão desktop e mobile, seguido do styleguide com todos os elementos do projeto. 

Para um melhor entendimento da proposta, abaixo seguem também os links do Protótipo Navegável, onde você poderá ver as animações e interações que deverão estar presentes no resultado final.

O teste é basicamente duas sections com dois diferentes carrosséis. O primeiro simula um carrossel de vídeos com orientação vertical. O segundo simula um carrossel de colunistas com orientação horizontal (no protótipo o carrossel não funciona de forma literal). 

Atenção: No protótipo desktop você perceberá que o carrossel colunista possui um hover que contém  informações escondidas do layout. 

### Protótipo navegável

- Desktop: https://www.figma.com/proto/CA648bYoiXCDCLaQqqmtsp/Teste-Vaga-Front-End?page-id=5%3A784&node-id=5%3A905&viewport=711%2C735%2C0.39&scaling=min-zoom&starting-point-node-id=5%3A905
- Mobile: https://www.figma.com/proto/CA648bYoiXCDCLaQqqmtsp/Teste-Vaga-Front-End?page-id=6%3A493&node-id=6%3A495&viewport=712%2C963%2C0.57&scaling=min-zoom

### Arquivos Figma

- Styleguide: https://www.figma.com/file/CA648bYoiXCDCLaQqqmtsp/Teste-Vaga-Front-End?node-id=0%3A1
- Desktop: https://www.figma.com/file/CA648bYoiXCDCLaQqqmtsp/Teste-Vaga-Front-End?node-id=5%3A784
- Mobile: https://www.figma.com/file/CA648bYoiXCDCLaQqqmtsp/Teste-Vaga-Front-End?node-id=6%3A493

## Entrega final

A entrega final será composta por dois artefatos:

1. Enviar, via email rh@seox.com.br, o link do repositório originado deste (fork) com os códigos da solução implementada.
2. Utilize o Gitlab pages para hospedar a solução implementada. Nosso setor de QA (Quality Assurance) vai utilizar essa página para avaliar o seu trabalho visual.

## O que será avaliado

1. Capacidade de interpretação de briefing e relação da proposta com o briefing.
2. Qualidade estética e execução do resultado
3. Organização dos códigos, componentização e responsividade
4. SEO Técnico (CWV)

## Diretrizes

1. O teste deverá ser desenvolvido utilizando SASS
2. Poderá ser utilizado qualquer Framework Frontend para auxiliar no desenvolvimento (Ex: Bootstrap ou Tailwind).
3. Inserir comentários inteligentes para diferenciação de componentes

## Observações importantes

- Vamos avaliar também o seu capricho com o Git.
- Caso você tenha algum problema com a entrega, por favor entrar em contato pelo email rh@seox.com.br.
- Nosso time está disponível para tirar dúvidas. Para isso, entre em contato pelo email rh@seox.com.br.

## Notas extras

- Nosso time de QA visual é conhecido como "Pixel Hunters". Capriche na fidelidade com o Figma.
