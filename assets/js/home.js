// FUNÇÃO PARA ALTERAR O CONTEÚDO DO VÍDEO DESTAQUE
function changeHighlight(e) {
  let highlight = document.querySelector('.highlight')
  thumbnail = highlight.querySelector('.highlightImage')
  subheadedr = highlight.querySelector('.text h3')
  title = highlight.querySelector('.text h2')

  if (highlight.classList.contains('fade-in')) {
    highlight.classList.remove('fade-in')
  }

  highlight.classList.add('fade-in')
  console.log(e.getAttribute("data-thumbnail"))
  let newThumbnail = e.getAttribute("data-thumbnail")
  let newTitle = e.getAttribute("data-title")
  let newSubheader = e.getAttribute("data-subheader")

  thumbnail.setAttribute('src', newThumbnail)
  title.innerHTML = newTitle
  subheadedr = newSubheader
}

// LOOPS DE CARROSSEL, PARA MONTAR QUANTOS FOREM NECESSÁRIO
for (var x of document.querySelectorAll('.splide.colunistas')) {
  var splide = new Splide(x, {
    autoWidth: true,
    gap: 24,
    focus: 'center',
    pauseOnFocus: true,
  });
  splide.mount();
  x.style.display = 'inherit';
}

// LOOPS DE CARROSSEL, PARA MONTAR QUANTOS FOREM NECESSÁRIO
for (var x of document.querySelectorAll('.splide.videosCarrossel')) {
  var splide = new Splide(x, {
    autoWidth: true,
    direction: 'ttb',
    height: '637px',
    gap: '24px',
    breakpoints: {
      850: {
        direction: 'LTR',
      },
    },
  });

  splide.mount();
  x.style.display = 'inherit';
} 